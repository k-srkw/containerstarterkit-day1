# はじめてのアプリケーションデプロイ

## アジェンダ
 1. CLI ログイン
 1. Project 作成
 1. アプリケーション作成
 1. アプリケーションの確認
 1. Liveness Probe 設定の追加
 1. オートヒールの動作の確認
 
## CLI ログイン
ssh クライアントから、踏み台マシン(以下、**bastion** )マシンへアクセスします。

```
本ハンズオンでは、踏み台マシンへのアクセスは共通アカウントを使用しております。
```

<!-- アクセス情報は、開催毎に環境にあわせて編集する -->

### bationへsshログイン

```
ssh　アクセス ( linux マシンの場合 )
$ ssh lab-user@bastion
* ユーザアカウント: lab-user
* パスワード: 
```

### bationマシンでの準備

openshiftにアクセスする際のユーザーアカウント名と同じ名前のディレクトリを作成し、環境変数HOME を変更します。


```
$ mkdir user1
```

#### 補足
```
OpenShift のCli セッションは、$HOME/.kubeconfig に保存されます。
本ハンズオンでは、Linuxユーザアカウントを参加者で共有するため、通常のLinux ログイン状態だとセッション情報が上書きされます。
これを回避するため、環境変数 HOME を変更し、競合を回避しています。
```


```
操作：アカウント：パスワードを入力し、ログインします。
```
![Console Login](./images/WS001.png)


### (ハンズオンの場合)ユーザアカウントの割り当て

<!-- Etherpad のURLを適宜編集  -->

```
講師によるガイド

環境上etherpad へ誘導し、アカウントにメールアドレスを記入してもらう

user1: toaraki@redhat.com
user2:
user3:
user4:
...

RHPDS workshop環境の共通パスワードは、openshift
ROSA の場合は、個別の人称方法に依存

```

#### 事前準備

* etherpad のデプロイ
* user1 - user'n' のリスとアップ

```
操作：Etherpad へアクセスして、user1 ... の箇所へ、メールアドレスを入力してください。ユーザの重複はできないので、空いている箇所へ記入をお願いします。
```


``` 画像で置き換え
...
user1: toaraki@redhat.com
user2:
user3:
user4:
...

```

## developer View の確認
ログインが完了すると、Developer View が表示されます。

```
操作：ツアーガイドが表示される場合、スキップしてください.画面右上の'?' アイコンから"ガイド付きツアー"を呼び出すことでからいつでも表示できます.
```

![Console Login](./images/WS002.png)

* 参考: ガイド付きツアー を任意に実行する
![Console Login](./images/WS003.png)

* 参考: ２回目以降のログイン等の事情で、Administrator ビューが表示される場合は、左上部分のメニューより切り替えを行ってください。

![Console Login](./images/WS003-2.png)

## Project 作成
まず、プロジェクトを作成します。任意の名称が指定できますが、今回は、アカウント-webapp (ex: user1 の場合、user1-webapp) としてください。

```
操作：画面中央部の"プロジェクト:すべてのプロジェクト" と表示されているドロップダウンリストを選択し、リスト最下部の"プロジェクト作成"ボタンを押下します。
```

<!-- 画像 -->
![Console Login](./images/WS004.png)

```
操作:プロジェクトの作成ウィンドウにて、"名前" フィールドに、アカウント-webapp (ex: user1 の場合、user1-webapp) を入力し、作成ボタンを押下します。
```

<!-- 画像 -->
![Console Login](./images/WS006-2.png)

```
確認：画面中央部の"プロジェクト:アカウント-webapp" となっていることを確認します。
```

<!-- 画像 -->
![Console Login](./images/WS007.png)

```
操作： 左メニューから、"+追加" を選択
```
![Console Login](./images/WS007.png)

## アプリケーション作成
Python カタログを使用して、アプリケーションを作成します。

### 開発カタログ　
```
操作："すべてのサービス" を押下します。
```
![Console Login](./images/WS007-2.png)

### 開発カタログの選択
```
操作："開発カタログ"画面の"キーワードでフィルター"に、`python` と入力し、カタログのリスとにフィルターを適用します。
```
![Console Login](./images/WS008.png)


```
操作：ビルダーイメージ：Pythonを押下します。
```
![Console Login](./images/WS008-2.png)

```
操作：カタログの説明が表示されるので、"アプリケーションの作成"ボタンを押下します。
```
![Console Login](./images/WS009.png)

```
操作：Source-to-Image(S2I) アプリケーションの作成　画面の GitリポジトリーURLに、
"https://github.com/openshift-katacoda/blog-django-py"
を入力し、画面下部の作成ボタンを押下します。
```
![Console Login](./images/WS010-1.png)

```
確認：画面がトポロジービューに変わるので、デプロイが完了するまで待ちます。
```
![Console Login](./images/WS011.png)

Console の画面を探索し、どのような情報が参照できるか確認してください。

* Deploy ステータスの確認
```
Pod のアイコンを押下すると、右側に Deploymentの定義情報等を表示する画面が表示されます。
```
![Console Login](./images/WS012.png)

* リソースの状態の確認
```
Deployment情報表示部の内部タブ"リソース"を選択すると、リソースの状態を確認できます。
この資料の画像は、ビルド処理中となります。
```
![Console Login](./images/WS013.png)

* ビルドログの確認
```
リソースタブ内の"ビルド"セクションにある"ログの表示"リンクを押下すると、ビルドログを確認することができます。
```
![Console Login](./images/WS014.png)

* デプロイの完了
```
Pod のデプロイ処理が完了すると、トポロジービューのアイコンの表示が、濃い青枠で囲まれます。
```
![Console Login](./images/WS015.png)


## アプリケーションの確認
トポロジービューのアプリケーションアイコン右上の記号から、アプリケーションへアクセスできます。
![Console Login](./images/WS017.png)


## Liveness Probe 設定の追加
コンテナアプリの監視設定を行ないます。

```
操作：トポロジービューにて、Deployment 情報画面を開き（Pod のアイコンを押下 )、情報画面の上部に表示されている"ヘルスチェック"セクションにある
"ヘルスチェックの追加"リンクを押下します。
```
![Console Login](./images/WS015.png)

```
操作："Liveness プローブの追加" ボタンを押下します。
```
![Console Login](./images/WS018.png)

```
操作：(今回はデフォルトの設定を使用します。)展開されたセクションの最下部にある"ㇾ"チェックを押下します。
```
![Console Login](./images/WS019-1.png)
(画面の続き)
![Console Login](./images/WS019-2.png)

```
操作：Liveness プローブの表示が、"追加済みのLivenessプローブ" (緑色の文字)となっていることを確認し、画面下部の"追加" ボタンを押下します。
```
![Console Login](./images/WS019-3.png)

```
確認：Pod の作り直し操作が完了するのを待ちます。
```
![Console Login](./images/WS020.png)

## オートヒールの動作の確認
擬似的に障害を発生させて、復旧されることを確認します。前節のLiveness プローブの設定が済んでない場合は、ここでの操作を行なったとしても回復は行なわれないので注意してください。

擬似的な障害として、コンテナへターミナルアクセスし、アプリケーションの構成ファイルを削除します。これによって、コンテナは、HTTPリターンコード 403 を返しますので、OpenShift のLiveness プローブが検出ししだい、自動的に復旧が行なわれます。

```
確認: トポロジービューのアプリケーションアイコン右上の記号から、アプリケーションへアクセスし、Blog アプリの画面が表示することを確認します。
```
![Console Login](./images/WS017.png)

```
操作：OpenShift のコンソールに戻り、トロポジービューからDeployment の情報を開きます。
続けて、Deployment 情報のPod セクションから、Pod 名（青文字表記のリンク)を押下します。
```
![Console Login](./images/WS015.png)

```
操作：Pod の詳細画面にて、"ターミナル"タブを選択します。
```
![Console Login](./images/WS021.png)

```
操作：擬似的に障害をおこします。ターミナルのシェル(コマンドライン)にて、下記のコマンドを入力します。
$ pwd 
/opt/app-root/src  
   ## カレントディレクトリが上記であることを確認.
$ ls
app.sh blog ...
   ## ファイルやディレクトリがあることを確認
$ rm -rf *
$ ls
  ## ファイルが削除されていることを確認
```
![Console Login](./images/WS022.png)
![Console Login](./images/WS023.png)

```
確認: Blog アプリケーションへアクセスし、"Forbidden (HTTP 403 )" が返されることを確認します。
```
![Console Login](./images/WS024.png)

```
操作: 10秒程度の間隔で画面をリフレッシュして、アプリケーションの画面表示が正常になることを確認してください。
```
![Console Login](./images/WS025.png)

```
操作：OpenShift Console のトポロジービューより、Deployment の情報画面を展開し、PodセクションにあるPodのリンクをたどります。
```
![Console Login](./images/WS026.png)


```
操作：Pod の詳細画面にて"イベント"タブを押下します。
```
![Console Login](./images/WS027.png)

```
確認：Pod のイベント一覧の中に、Liveness Prove の失敗や、その後のリスタートのイベントが記録されていることを確認します。
表示の内容から、障害が検出され、回復が行なわれたことが分かります。
```
![Console Login](./images/WS021.png)


